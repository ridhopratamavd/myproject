package com.example.myapplication;

import java.util.ArrayList;
import java.util.List;

public class DataSource {
    public static List<Task> createTasksList(){
        List<Task> tasks = new ArrayList<>();
        tasks.add(new Task("task 1", true, 3));
        tasks.add(new Task("task 2", false, 2));
        tasks.add(new Task("task 3", true, 0));
        tasks.add(new Task("task 4", false, 5));
        return tasks;
    }
}
