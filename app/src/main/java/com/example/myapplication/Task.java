package com.example.myapplication;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Task {
    private String description;
    private boolean complete;
    private int priority;
}
