package com.example.myapplication.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.myapplication.model.Word;

import java.util.List;

/*
 * maps method calls to database queries,
 * so that when the Repository calls a method such as getAlphabetizedWords(),
 * Room can execute SELECT * from word_table ORDER BY word ASC
 * */
@Dao
public interface WordDAO {

    // allowing the insert of the same word multiple times by passing a
    // conflict resolution strategy
    @Insert()
    void insert(Word word);

    @Query("DELETE FROM word_table")
    void deleteAll();

    @Query("SELECT * from word_table ORDER BY word ASC")
    LiveData<List<Word>> getAlphabetizedWords();
}