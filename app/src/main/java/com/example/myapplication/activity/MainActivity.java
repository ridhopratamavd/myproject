package com.example.myapplication.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.adapter.WordListAdapter;
import com.example.myapplication.database.WordViewModel;
import com.example.myapplication.model.Word;

import io.reactivex.rxjava3.disposables.CompositeDisposable;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "MainActivity";
    CompositeDisposable disposables = new CompositeDisposable();
    TextView textView;
    Button modifyButton, openAnotherActivityButton, dialNumberButton, showLocationButton, viewIntentButton, openChromeButton;
    Boolean clicked = false;
    private WordViewModel mWordViewModel;
    public static final int NEW_WORD_ACTIVITY_REQUEST_CODE = 1;

    /*
     * the activity is created
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindingAllElement();
        setElementListener();
        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        final WordListAdapter adapter = new WordListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mWordViewModel = new ViewModelProvider(this).get(WordViewModel.class);

        mWordViewModel.getAllWords().observe(this, words -> {
            // Update the cached copy of the words in the adapter.
            adapter.setWords(words);
        });
    }

    private void setElementListener() {
        modifyButton.setOnClickListener(this);
        dialNumberButton.setOnClickListener(this);
        showLocationButton.setOnClickListener(this);
        openAnotherActivityButton.setOnClickListener(this);
        openChromeButton.setOnClickListener(this);
        viewIntentButton.setOnClickListener(this);
    }

    private void bindingAllElement() {
        textView = findViewById(R.id.helloWorld);
        modifyButton = findViewById(R.id.modifyTextButton);
        openAnotherActivityButton = findViewById(R.id.openAnotherActivity);
        openChromeButton = findViewById(R.id.openChromeButton);
        showLocationButton = findViewById(R.id.showLocationButton);
        dialNumberButton = findViewById(R.id.dialNumberButton);
        viewIntentButton = findViewById(R.id.viewIntentButton);
    }

    /*
     * when activity launched*/
    @Override
    protected void onStart() {
        super.onStart();

    }

    /*
     * activity is in the foreground
     * and user can interact with it
     * */
    @Override
    protected void onResume() {
        super.onResume();

    }

    /*
     * activity completely hidden and not visible to the user
     * it is considered  to be in the background*/
    @Override
    protected void onStop() {
        super.onStop();

    }

    /*
     * partially  obscured by another activity
     * activity in the foreground is semi transparent
     * */
    @Override
    protected void onPause() {
        super.onPause();

    }

    /*
     * activity  is destroyed and removed from them emory
     * */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposables.clear();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.dialNumberButton:
                Intent intentDial = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:081999663871"));
                startActivity(intentDial);
                break;
            case R.id.openAnotherActivity:
                Intent intent = new Intent(MainActivity.this, NewWordActivity.class);
                startActivityForResult(intent, NEW_WORD_ACTIVITY_REQUEST_CODE);
                break;
            case R.id.openChromeButton:
                Intent intentChrome = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.btpn.co.id"));
                startActivity(intentChrome);
                break;
            case R.id.showLocationButton:
                Intent intentShowLocation = new Intent(Intent.ACTION_VIEW, Uri.parse("geo: 12.9777343,77.24236643"));
                startActivity(intentShowLocation);
                break;
            case R.id.viewIntentButton:
                break;
            case R.id.modifyTextButton:
                this.clicked = !this.clicked;
                if (this.clicked) {
                    textView.setText(R.string.clicked);
                } else {
                    textView.setText(R.string.hello_world);
                }
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == NEW_WORD_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            Word word = Word.builder().word(data.getStringExtra(NewWordActivity.EXTRA_REPLY)).build();
            mWordViewModel.insert(word);
        } else if (requestCode == NEW_WORD_ACTIVITY_REQUEST_CODE && resultCode == RESULT_CANCELED) {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.contain_invalid_char,
                    Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.not_saved_empty_char,
                    Toast.LENGTH_LONG).show();
        }
    }
}

/*
START

2020-05-17 22:21:26.823 6470-6470/? D/MainActivity: DEBUK on create
2020-05-17 22:21:26.971 6470-6470/? D/MainActivity: DEBUK on subscribe
2020-05-17 22:21:26.975 6470-6470/? D/MainActivity: DEBUK on start
2020-05-17 22:21:26.977 6470-6470/? D/MainActivity: DEBUK on resume
2020-05-17 22:21:26.990 6470-6542/? D/MainActivity: DEBUK on background : RxCachedThreadScheduler-1
2020-05-17 22:21:26.990 6470-6542/? D/MainActivity: DEBUK on background : sleep done
2020-05-17 22:21:26.990 6470-6542/? D/MainActivity: DEBUK on background : result : true
2020-05-17 22:21:26.990 6470-6542/? D/MainActivity: DEBUK on background : RxCachedThreadScheduler-1
2020-05-17 22:21:26.990 6470-6542/? D/MainActivity: DEBUK on background : sleep done
2020-05-17 22:21:26.990 6470-6542/? D/MainActivity: DEBUK on background : result : false
2020-05-17 22:21:26.990 6470-6542/? D/MainActivity: DEBUK on background : RxCachedThreadScheduler-1
2020-05-17 22:21:26.990 6470-6542/? D/MainActivity: DEBUK on background : sleep done
2020-05-17 22:21:26.990 6470-6542/? D/MainActivity: DEBUK on background : result : true
2020-05-17 22:21:26.990 6470-6542/? D/MainActivity: DEBUK on background : RxCachedThreadScheduler-1
2020-05-17 22:21:26.990 6470-6542/? D/MainActivity: DEBUK on background : sleep done
2020-05-17 22:21:26.990 6470-6542/? D/MainActivity: DEBUK on background : result : false
2020-05-17 22:21:27.000 6470-6470/? D/MainActivity: DEBUK onNext: thread : main
2020-05-17 22:21:27.000 6470-6470/? D/MainActivity: DEBUK onNext: : Take out the trash
2020-05-17 22:21:27.000 6470-6470/? D/MainActivity: DEBUK onNext: thread : main
2020-05-17 22:21:27.000 6470-6470/? D/MainActivity: DEBUK onNext: : Unload the dishwasher
2020-05-17 22:21:27.000 6470-6470/? D/MainActivity: DEBUK on complete :

END
*/